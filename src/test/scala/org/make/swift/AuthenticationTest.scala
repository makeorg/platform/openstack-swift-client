/*
 * Copyright 2018 Make.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.make.swift

import org.apache.pekko.actor.typed.ActorSystem
import org.apache.pekko.actor.typed.scaladsl.Behaviors
import com.typesafe.scalalogging.StrictLogging
import org.make.swift.authentication.{AuthenticationRequest, Authenticator}
import org.scalatest.concurrent.PatienceConfiguration.Timeout

import scala.concurrent.duration.DurationInt

class AuthenticationTest extends BaseTest with DockerSwiftAllInOne with StrictLogging {

  implicit val system: ActorSystem[?] = ActorSystem(Behaviors.empty, "tests")
  override val externalPort: Int = 12345

  Feature("Swift all-in-one authentication") {
    Scenario("authenticate correctly") {
      val authenticator: Authenticator =
        Authenticator.newAuthenticator(Authenticator.KeystoneV1, s"http://${swiftContainerIp()}:$internalPort/auth/v1.0")

      whenReady(authenticator.authenticate(AuthenticationRequest("tester", "testing", "test")), Timeout(5.seconds)) {
        result =>
          logger.info("{}", result)
          result.tokenInfo.token.length should be >= 1
      }
    }

    Scenario("bad credentials") {
      val authenticator: Authenticator =
        Authenticator.newAuthenticator(Authenticator.KeystoneV1, s"http://${swiftContainerIp()}:$internalPort/auth/v1.0")

      whenReady(
        authenticator
          .authenticate(AuthenticationRequest("tester", "bad-credentials", "test"))
          .failed,
        Timeout(5.seconds)
      ) { e =>
        logger.error("", e)
      }
    }
  }

  override def afterStart(): Unit = {

  }

  override protected def afterAll(): Unit = {
    super.afterAll()
    containerManager.stopRmAll()
    system.terminate()
  }
}
