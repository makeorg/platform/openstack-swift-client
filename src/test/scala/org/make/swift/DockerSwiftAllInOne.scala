/*
 * Copyright 2018 Make.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.make.swift

import com.spotify.docker.client.messages.PortBinding
import com.whisk.docker.testkit.scalatest.DockerTestKitForAll
import com.whisk.docker.testkit.{Container, ContainerSpec, DockerReadyChecker, DockerTestTimeouts, ManagedContainers, SingleContainer}
import org.scalatest.Suite

import scala.concurrent.duration.DurationInt

trait DockerSwiftAllInOne extends DockerTestKitForAll {
  self: Suite =>

  final val internalPort = 8080
  def externalPort: Int

  val swiftContainerName = getClass.getSimpleName

  final val swiftContainer: ContainerSpec =
    ContainerSpec(image = "bouncestorage/swift-aio:latest")
      .withName(swiftContainerName)
      .withPortBindings((internalPort, PortBinding.of(null, externalPort)))
      .withReadyChecker(DockerReadyChecker.LogLineContains("supervisord started with pid"))

  override val dockerTestTimeouts: DockerTestTimeouts = DockerTestTimeouts.Default.copy(
    init = 5.minutes,
    stop = 1.minute
  )

  override val managedContainers: ManagedContainers = SingleContainer(new Container(swiftContainer))

  def swiftContainerIp(): String =
    dockerClient.inspectContainer(swiftContainerName).networkSettings().ipAddress()
}
